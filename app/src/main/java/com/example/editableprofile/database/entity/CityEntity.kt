package com.example.editableprofile.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Entity class for city_table
 * @author slakra
 * @date 09/02/19
 */
@Entity(tableName = "city_table")
class CityEntity {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    @ColumnInfo(name = "city")
    var city: String = ""

    @ColumnInfo(name = "lat")
    var lat: String = ""

    @ColumnInfo(name = "lon")
    var lon: String = ""
}