package com.example.editableprofile.database.repository

import androidx.lifecycle.LiveData
import com.example.editableprofile.apiManager.ApiManager
import com.example.editableprofile.database.dao.ChoiceDao
import kotlinx.coroutines.ExperimentalCoroutinesApi

/**
 * @brief: Repository class for City Entity
 * @author Sumit Lakra
 * @date 09/02/2019
 */
class ChoiceRepository(val choiceDao: ChoiceDao, val apiManager: ApiManager) {

    /**
     * Function to get category choice list from db
     * @author Sumit Lakra
     * @return [LiveData<List<String>>]
     * @date 09/02/2019
     */
    fun getChoiceList(category: String): LiveData<List<String>> = choiceDao.getCategoryList(category)

    /**
     * Function to get all single choices from server
     * @author slakra
     * @date 09/07/19
     */
    @ExperimentalCoroutinesApi
    suspend fun getChoices(): Boolean = apiManager.getSingleChoices()

    /**
     * Function to clear previous choice data from DB
     * @author slakra
     * @date 09/07/19
     */
    fun clearPreviousChoices() = choiceDao.deleteAllChoices()
}
