package com.example.editableprofile.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.example.editableprofile.database.entity.CityEntity
import com.example.editableprofile.model.City

/**
 * @brief: City Dao to access city_table
 * @author Sumit Lakra
 * @date 09/02/2019
 */
@Dao
abstract class CityDao : RoomBaseDao<CityEntity>() {

    /**
     * Function to get City list from Db
     * @return [LiveData<List<City>>]
     * @author slakra
     * @date 09/03/19
     */
    @Query("SELECT city, lat, lon FROM city_table")
    abstract fun getCityList(): LiveData<List<City>>

    /**
     * @brief: Function to delete all cities
     * @author Sumit Lakra
     * @date 09/03/2019
     */
    @Query("DELETE FROM city_table")
    abstract fun deleteAllCities()
}