package com.example.editableprofile.database.helper

import com.example.editableprofile.database.entity.ChoiceEntity
import com.example.editableprofile.database.entity.CityEntity
import com.example.editableprofile.model.ChoiceAttr
import com.example.editableprofile.model.City

/**
 * Extension method for CityEntity conversion
 * @return [City]
 * @author slakra
 * @date 09/03/19
 */
fun CityEntity.toModel(): City = City(
    lat = lat, lon = lon, city = city
)

/**
 * Extension method for City to CityEntityConversion
 * @return [CityEntity]
 * @author slakra
 * @date 09/03/19
 */
fun City.toDBEntity(): CityEntity {
    val en = CityEntity()
    en.city = this.city
    en.lat = this.lat
    en.lon = this.lon

    return en
}

/**
 * Extension method for ChoiceEntity conversion
 * @return [ChoiceAttr]
 * @author slakra
 * @date 09/03/19
 */
fun ChoiceEntity.toModel(): ChoiceAttr = ChoiceAttr(
    id = categoryId, name = name
)

/**
 * Extension method for ChoiceAttr to ChoiceEntity
 * @return [ChoiceEntity]
 * @author slakra
 * @date 09/03/19
 */
fun ChoiceAttr.toDBEntity(category: String): ChoiceEntity {
    val en = ChoiceEntity()
    en.name = this.name
    en.categoryId = this.id
    en.category = category

    return en
}
