package com.example.editableprofile.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.editableprofile.database.dao.ChoiceDao
import com.example.editableprofile.database.dao.CityDao
import com.example.editableprofile.database.entity.ChoiceEntity
import com.example.editableprofile.database.entity.CityEntity

/**
 * @brief: This class represents the RoomDatabase class from android. It contains abstract methods to
 * to get the Dao objects corresponding to each table . see DataBaseModule class for getting the instance
 * of this class.
 * @author Sumit Lakra
 * @date: 09/02/2019
 */
@Database(
    entities = [
    ChoiceEntity::class,
    CityEntity::class
    ], version = 1, exportSchema = false
)
abstract class EditProfileDataBase : RoomDatabase() {

    /**
     * return cityDao instance
     * @return [CityDao]
     * @author Sumit Lakra
     * @date: 09/02/2019
     */
    abstract fun cityDao(): CityDao

    /**
     * return choiceDao instance
     * @return [ChoiceDao]
     * @author Sumit Lakra
     * @date: 09/02/2019
     */
    abstract fun choiceDao(): ChoiceDao
}