package com.example.editableprofile.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.example.editableprofile.database.entity.ChoiceEntity

/**
 * @brief: Choice Dao to access choice_table
 * @author Sumit Lakra
 * @date 09/02/2019
 */
@Dao
abstract class ChoiceDao : RoomBaseDao<ChoiceEntity>() {

    /**
     * @brief: Function to get category list
     * @param [category] String
     * @return [LiveData<List<String>>]
     * @author Sumit Lakra
     * @date 09/02/2019
     */
    @Query("SELECT name FROM choice_table WHERE category = :category")
    abstract fun getCategoryList(category: String): LiveData<List<String>>

    /**
     * @brief: Function to delete all category list
     * @author Sumit Lakra
     * @date 09/03/2019
     */
    @Query("DELETE FROM choice_table")
    abstract fun deleteAllChoices()
}