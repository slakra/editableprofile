package com.example.editableprofile.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Entity class for choice_table
 * @author slakra
 * @date 09/02/19
 */
@Entity(tableName = "choice_table")
class ChoiceEntity {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    @ColumnInfo(name = "category")
    var category: String = ""

    @ColumnInfo(name = "cat_id")
    var categoryId: String = ""

    @ColumnInfo(name = "name")
    var name: String = ""
}