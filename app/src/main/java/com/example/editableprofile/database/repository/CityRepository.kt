package com.example.editableprofile.database.repository

import androidx.lifecycle.LiveData
import com.example.editableprofile.apiManager.ApiManager
import com.example.editableprofile.database.dao.CityDao
import com.example.editableprofile.model.City
import kotlinx.coroutines.ExperimentalCoroutinesApi

/**
 * @brief: Repository class for City Entity
 * @author Sumit Lakra
 * @date 09/02/2019
 */
class CityRepository(val cityDao: CityDao, val apiManager: ApiManager) {

    /**
     * Function to get city list from db
     * @author Sumit Lakra
     * @return [LiveData<List<String>>]
     * @date 09/02/2019
     */
    fun getCityList(): LiveData<List<City>> = cityDao.getCityList()

    /**
     * Function to get city list from server
     * @author slakra
     * @date 09/07/19
     */
    @ExperimentalCoroutinesApi
    suspend fun getCities(): Boolean = apiManager.getCities()
}
