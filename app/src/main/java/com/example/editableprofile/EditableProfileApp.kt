package com.example.editableprofile

import android.app.Application
import com.example.editableprofile.di.appModule
import com.orhanobut.hawk.Hawk
import com.squareup.leakcanary.LeakCanary
import org.koin.android.ext.android.startKoin

/**
 * @brief: Class that extends Application class. It contains all the application level initializations.
 * @author slakra
 * @date 09/02/19
 */
class EditableProfileApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
        initLeakCanary()
        initHawk()
    }

    private fun initKoin() {
        startKoin(this, appModule)
    }

    private fun initHawk() {
        Hawk.init(this).build()
    }

    /**
     * Function to configure LeakCanary.
     */
    private fun initLeakCanary() {
        if (LeakCanary.isInAnalyzerProcess(this)) {
            //  This process is dedicated to LeakCanary for heap analysis.
            //  You should not init your app in this process.
            return
        }
        LeakCanary.install(this)
    }
}