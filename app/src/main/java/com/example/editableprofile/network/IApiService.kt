package com.example.editableprofile.network

import com.example.editableprofile.model.response.CityResponse
import com.example.editableprofile.model.response.SingleChoiceResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

/**
 * Interface for APi end points
 * @author Sumit Lakra
 * @date 09/02/19
 */
interface IApiService {

    /**
     * Function to get city list from server
     * @return [Deferred<CityResponse>]
     * @author slakra
     * @date 09/03/19
     */
    @GET("locations/cities.json")
    fun getCities(): Deferred<CityResponse>

    /**
     * Function to get city list from server
     * @return [Deferred<SingleChoiceResponse>]
     * @author slakra
     * @date 09/03/19
     */
    @GET("single_choice_attributes.json")
    fun getSingleChoices(): Deferred<SingleChoiceResponse>
}