package com.example.editableprofile.ui.view

import android.os.Bundle
import androidx.lifecycle.Observer
import com.example.editableprofile.BR
import com.example.editableprofile.R
import com.example.editableprofile.common.ViewType
import com.example.editableprofile.common.util.Constants
import com.example.editableprofile.common.util.toast
import com.example.editableprofile.components.BaseActivity
import com.example.editableprofile.components.IRouter
import com.example.editableprofile.databinding.ActivityViewProfileBinding
import com.example.editableprofile.model.EventIdentifier
import com.example.editableprofile.ui.viewModel.ViewProfileViewModel
import com.google.firebase.firestore.ListenerRegistration
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Class for showing Profile details
 * @author slakra
 * @date 09/04/19
 */
class ViewProfileActivity : BaseActivity<ActivityViewProfileBinding, ViewProfileViewModel>() {

    val viewProfileViewModel: ViewProfileViewModel by viewModel()
    private lateinit var profileListener: ListenerRegistration
    private val router: IRouter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setActionBar()
        subscribeObservers()
    }

    override fun onStart() {
        super.onStart()
        viewProfileViewModel.checkProfileStatus()
        profileListener = viewProfileViewModel.getProfileDetails()
    }

    override fun onStop() {
        super.onStop()
        profileListener.remove()
    }

    override fun getLayoutId(): Int = R.layout.activity_view_profile

    override fun getViewModel(): ViewProfileViewModel = viewProfileViewModel

    override fun getBindingVariable(): Int = BR.viewModel

    private fun setActionBar() {
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "View Profile"
    }

    @Suppress("ComplexMethod")
    private fun subscribeObservers() {
        viewProfileViewModel.onEventReceived += { event ->
            when (event.type) {
                EventIdentifier.EDIT_PROFILE_CLICKED ->
                    router.routeTo(this@ViewProfileActivity, ViewType.EDIT_PROFILE,
                    Bundle().apply {
                        putBoolean(Constants.IS_CREATE_PROFILE, false)
                    })
                EventIdentifier.CREATE_PROFILE_CLICKED ->
                    router.routeTo(this@ViewProfileActivity, ViewType.EDIT_PROFILE,
                        Bundle().apply {
                            putBoolean(Constants.IS_CREATE_PROFILE, true)
                        })
                else -> {
                    // Do Nothing
                }
            }
        }

        viewProfileViewModel.userProfileData.observe(this, Observer {
            getViewDataBinding().profileData = it
        })

        viewProfileViewModel.waitingDialogMsg.observe(this, Observer { msg ->
            if (msg == null) {
                hideWaitingDialog()
            } else {
                if (msg.startsWith("Getting", true)) {
                    showWaitingDialog(msg)
                } else {
                    hideWaitingDialog()
                    toast(msg)
                }
            }
        })
    }
}
