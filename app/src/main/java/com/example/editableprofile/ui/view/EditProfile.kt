package com.example.editableprofile.ui.view

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.example.editableprofile.BR
import com.example.editableprofile.R
import com.example.editableprofile.common.util.Constants
import com.example.editableprofile.common.util.toast
import com.example.editableprofile.components.BaseActivity
import com.example.editableprofile.databinding.ActivityEditProfileBinding
import com.example.editableprofile.model.City
import com.example.editableprofile.model.EventIdentifier
import com.example.editableprofile.ui.viewModel.EditProfileViewModel
import java.util.Calendar
import kotlin.collections.ArrayList
import kotlinx.android.synthetic.main.activity_edit_profile.ethnicity_spinner
import kotlinx.android.synthetic.main.activity_edit_profile.figure_spinner
import kotlinx.android.synthetic.main.activity_edit_profile.gender_spinner
import kotlinx.android.synthetic.main.activity_edit_profile.location_actv
import kotlinx.android.synthetic.main.activity_edit_profile.marital_spinner
import kotlinx.android.synthetic.main.activity_edit_profile.religion_spinner
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Edit profile activity
 * @author slakra
 * @date 09/07/19
 */
class EditProfile : BaseActivity<ActivityEditProfileBinding, EditProfileViewModel>(),
    AdapterView.OnItemSelectedListener {

    val mViewModel: EditProfileViewModel by viewModel()
    private val cityList = ArrayList<City>()
    private val genderList = ArrayList<String>()
    private val religionList = ArrayList<String>()
    private val ethnicityList = ArrayList<String>()
    private val figureList = ArrayList<String>()
    private val maritalStatusList = ArrayList<String>()
    private val cityNames = ArrayList<String>()
    private var permissionGranted = false

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel.isCreateProfile.set(
            intent?.extras?.getBoolean(Constants.IS_CREATE_PROFILE) ?: false
        )
        setActionBar()
        subscribeObservers()
        if (!mViewModel.isCreateProfile.get())
            mViewModel.getProfileDetails()
    }

    override fun onResume() {
        super.onResume()
        mViewModel.isOnline = isNetworkAvailable()
    }

    override fun getLayoutId(): Int = R.layout.activity_edit_profile

    override fun getViewModel(): EditProfileViewModel = mViewModel

    override fun getBindingVariable(): Int = BR.viewModel

    private fun setActionBar() {
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = if (mViewModel.isCreateProfile.get()) {
            "Create New Profile"
        } else {
            "Edit Profile"
        }
    }

    @Suppress("LongMethod", "ComplexMethod")
    @ExperimentalCoroutinesApi
    private fun subscribeObservers() {

        mViewModel.onEventReceived += { event ->
            when (event.type) {
                EventIdentifier.SHOW_BIRTHDAY_PICKER -> showPicker()
                EventIdentifier.PROFILE_SUCCESS -> this@EditProfile.finish()
                EventIdentifier.PROFILE_FAILURE -> toast("Unknown error creating profile")
                EventIdentifier.PROFILE_OFFLINE -> toast("Can not create profile in offline mode")
                EventIdentifier.PROFILE_FETCH_ERROR -> toast("Error getting profile details")
                EventIdentifier.SELECT_PICTURE -> selectProfilePic()
                else -> {
                    // Do nothing
                }
            }
        }

        mViewModel.waitingDialogMsg.observe(this, Observer { msg ->
            if (msg != null) {
                showWaitingDialog(msg)
            } else {
                hideWaitingDialog()
            }
        })

        mViewModel.getGenderList().observe(this, Observer { list ->
            if (list.isNotEmpty()) {
                genderList.clear()
                genderList.addAll(list)

                val genderAdapter =
                    ArrayAdapter(this, android.R.layout.simple_spinner_item, genderList)
                genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                gender_spinner.adapter = genderAdapter
                if (mViewModel.gender.get() != "") {
                    val position = genderAdapter.getPosition(mViewModel.gender.get())
                    gender_spinner.setSelection(position)
                }

                gender_spinner.onItemSelectedListener = this
            } else {
                mViewModel.getSingleChoices()
            }
        })

        mViewModel.getEthnicityList().observe(this, Observer { ethList ->
            ethnicityList.clear()
            ethnicityList.addAll(ethList)
            val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, ethnicityList)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            ethnicity_spinner.adapter = adapter

            if (mViewModel.ethnicity.get() != "") {
                val position = adapter.getPosition(mViewModel.ethnicity.get())
                ethnicity_spinner.setSelection(position)
            }
            ethnicity_spinner.onItemSelectedListener = this
        })

        mViewModel.getFigureList().observe(this, Observer { list ->
            figureList.clear()
            figureList.addAll(list)
            val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, figureList)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            figure_spinner.adapter = adapter

            if (mViewModel.figure.get() != "") {
                val position = adapter.getPosition(mViewModel.figure.get())
                figure_spinner.setSelection(position)
            }
            figure_spinner.onItemSelectedListener = this
        })

        mViewModel.getReligionList().observe(this, Observer { list ->
            religionList.clear()
            religionList.addAll(list)
            val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, religionList)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            religion_spinner.adapter = adapter

            if (mViewModel.religion.get() != "") {
                val position = adapter.getPosition(mViewModel.religion.get())
                religion_spinner.setSelection(position)
            }
            religion_spinner.onItemSelectedListener = this
        })

        mViewModel.getMaritalStatusList().observe(this, Observer { list ->
            maritalStatusList.clear()
            maritalStatusList.addAll(list)
            val adapter =
                ArrayAdapter(this, android.R.layout.simple_spinner_item, maritalStatusList)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            marital_spinner.adapter = adapter

            if (mViewModel.maritalStatus.get() != "") {
                val position = adapter.getPosition(mViewModel.maritalStatus.get())
                marital_spinner.setSelection(position)
            }
            marital_spinner.onItemSelectedListener = this
        })

        mViewModel.getCities().observe(this, Observer { list ->
            if (list.isNotEmpty()) {
                cityList.clear()
                cityList.addAll(list)
                for (city in cityList) {
                    cityNames.add(city.city)
                }
                val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, cityNames)
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                location_actv.setAdapter(adapter)

                location_actv.onItemClickListener =
                    AdapterView.OnItemClickListener { p0, p1, position, p3 ->
                        val city = cityList[position]
                        mViewModel.location.set("${city.lat}, ${city.lon}")
                    }
            } else {
                mViewModel.getCityListFromNetwork()
            }
        })
    }

    private fun selectProfilePic() {
        if (checkPermission()) {
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            intent.type = "image/jpeg"
            startActivityForResult(
                Intent.createChooser(intent, "Select Profile Pic"),
                Constants.PICK_IMAGE
            )
        }
    }

    private fun showPicker() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val picker = DatePickerDialog(this,
            DatePickerDialog.OnDateSetListener { datePicker, y, m, d ->
                mViewModel.birthday.set("$d/${m + 1}/$y")
            }, year, month, day
        )

        picker.show()
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        // Do nothing
    }

    override fun onItemSelected(p0: AdapterView<*>, p1: View?, position: Int, p3: Long) {
        when (p0.id) {
            R.id.gender_spinner -> mViewModel.gender.set(genderList[position])
            R.id.ethnicity_spinner -> mViewModel.ethnicity.set(ethnicityList[position])
            R.id.figure_spinner -> mViewModel.figure.set(figureList[position])
            R.id.religion_spinner -> mViewModel.religion.set(religionList[position])
            R.id.marital_spinner -> mViewModel.maritalStatus.set(maritalStatusList[position])
            else -> {
                // Do nothing
            }
        }
    }

    @Suppress("UnsafeCallOnNullableType")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.PICK_IMAGE && resultCode == Activity.RESULT_OK &&
            data != null
        ) {
            val uri = data.data
            val projection = arrayOf(MediaStore.Images.Media.DATA)
            val cursor = contentResolver.query(uri!!, projection, null,
                null, null)
            cursor?.moveToFirst()
            val columnIndex = cursor?.getColumnIndex(projection[0])
            val picturePath = cursor?.getString(columnIndex!!)
            mViewModel.profileUrl.set(picturePath)
            mViewModel.uploadProfileImage()
            cursor?.close()
        }
    }

    @Suppress("OptionalWhenBraces")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Constants.READ_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    permissionGranted = true
                    selectProfilePic()
                } else {
                    toast("Permission denied")
                }
            }
            else -> {
            }
        }
    }

    private fun checkPermission(): Boolean {
        permissionGranted = if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.READ_EXTERNAL_STORAGE
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), Constants.READ_STORAGE
            )
            false
        } else {
            true
        }
        return permissionGranted
    }
}
