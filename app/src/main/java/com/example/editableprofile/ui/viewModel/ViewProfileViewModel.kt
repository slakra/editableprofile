package com.example.editableprofile.ui.viewModel

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import com.example.editableprofile.common.util.Constants
import com.example.editableprofile.common.util.PreferenceUtil
import com.example.editableprofile.components.BaseViewModel
import com.example.editableprofile.model.EventIdentifier
import com.example.editableprofile.model.UserProfile
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration

/**
 * ViewModel class for ViewProfile Activity
 * @author slakra
 * @date 09/04/19
 */
class ViewProfileViewModel(private val mFirestore: FirebaseFirestore) : BaseViewModel() {

    var userProfileData = MutableLiveData<UserProfile>()
    var waitingDialogMsg = MutableLiveData<String>()
    private var userId: String = "-1"
    val isProfileExists = ObservableBoolean(false)

    /**
     * Function to check if Profile exists
     * @author slakra
     * @date 09/08/19
     */
    fun checkProfileStatus() {
        userId = PreferenceUtil.userId ?: "-1"
        isProfileExists.set(userId != "-1")
    }

    /**
     * Function to handle EditProfile button click
     * @author slakra
     * @date 09/04/19
     */
    fun onEditProfileClick() {
        triggerEvent(EventIdentifier.EDIT_PROFILE_CLICKED)
    }

    /**
     * Function to handle CreateProfile button click
     * @author slakra
     * @date 09/04/19
     */
    fun onCreateProfileClicked() {
        triggerEvent(EventIdentifier.CREATE_PROFILE_CLICKED)
    }

    /**
     * Function to get profile Details
     * @return [ListenerRegistration]
     * @author slakra
     * @date 09/04/19
     */
    fun getProfileDetails(): ListenerRegistration {
        waitingDialogMsg.value = "Getting profile Details"
        return mFirestore.collection(Constants.USER).document(userId)
            .addSnapshotListener { snapshot, e ->
                if (e != null) {
                    waitingDialogMsg.value = e.message
                    return@addSnapshotListener
                }

                if (snapshot != null && snapshot.exists()) {
                    isProfileExists.set(true)
                    val profileData = snapshot.toObject(UserProfile::class.java)
                    userProfileData.postValue(profileData)
                    waitingDialogMsg.value = null
                } else {
                    isProfileExists.set(false)
                    waitingDialogMsg.value = null
                }
            }
    }
}
