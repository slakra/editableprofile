package com.example.editableprofile.ui.view

import androidx.test.espresso.IdlingResource

/**
 * EspressoIdlingResource class to help in UI testing
 * @author slakra
 * @date 09/11/19
 */
object EspressoIdlingResource {

    private const val RESOURCE = "GLOBAL"
    private val countingIdlingResource =
        SimpleIdlingResource(RESOURCE)

    /**
     * Function to increment count
     * @author slakra
     * @date 09/11/19
     */
    fun increment() =
        countingIdlingResource.incrementCounter()

    /**
     * Function to increment count
     * @author slakra
     * @date 09/11/19
     */
    fun decrement() =
        countingIdlingResource.decrementCounter()

    /**
     * Function to getIdlingResource
     * @return [IdlingResource]
     * @author slakra
     * @date 09/11/19
     */
    fun getIdlingResource(): IdlingResource = countingIdlingResource
}
