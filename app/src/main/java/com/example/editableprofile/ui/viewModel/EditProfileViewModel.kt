package com.example.editableprofile.ui.viewModel

import android.net.Uri
import android.text.TextUtils
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.editableprofile.R
import com.example.editableprofile.common.util.Constants
import com.example.editableprofile.common.util.PreferenceUtil
import com.example.editableprofile.components.BaseViewModel
import com.example.editableprofile.database.repository.ChoiceRepository
import com.example.editableprofile.database.repository.CityRepository
import com.example.editableprofile.model.City
import com.example.editableprofile.model.EventIdentifier
import com.example.editableprofile.model.UserProfile
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.StorageMetadata
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import java.io.File
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch

/**
 * View model for Edit Profile Activity
 * @author slakra
 * @date 09/07/19
 */
class EditProfileViewModel(
    private val choiceRepository: ChoiceRepository,
    private val cityRepository: CityRepository,
    private val mFireStore: FirebaseFirestore,
    private val mStorageReference: StorageReference
) : BaseViewModel() {

    val displayName = ObservableField<String>("")
    val realName = ObservableField<String>("")
    val birthday = ObservableField<String>("")
    val gender = ObservableField<String>("")
    val ethnicity = ObservableField<String>("")
    val religion = ObservableField<String>("")
    val height = ObservableField<String>("")
    val figure = ObservableField<String>("")
    val maritalStatus = ObservableField<String>("")
    val occupation = ObservableField<String>("")
    val aboutMe = ObservableField<String>("")
    val location = ObservableField<String>("")
    val profileUrl = ObservableField<String>("")

    val errorDisplayName = ObservableField<Int>()
    val errorRealName = ObservableField<Int>()
    val errorBirthday = ObservableField<Int>()
    val errorLocation = ObservableField<Int>()

    val isCreateProfile = ObservableBoolean(true)
    var isOnline = true
    var waitingDialogMsg = MutableLiveData<String>()
    private val userId = PreferenceUtil.userId ?: "-1"

    /**
     * Function to handle Edit button click
     * @author slakra
     * @date 09/07/19
     */
    @Suppress("UnsafeCallOnNullableType")
    fun editProfile() {
        resetError()
        if (validateFields()) {
            val userProfile = UserProfile()
            userProfile.displayName = displayName.get()!!
            userProfile.realName = realName.get()!!
            userProfile.birthday = birthday.get()!!
            userProfile.gender = gender.get()!!
            userProfile.ethnicity = ethnicity.get()
            userProfile.religion = religion.get()
            userProfile.height = "${height.get()} cm"
            userProfile.figure = figure.get()
            userProfile.maritalStatus = maritalStatus.get()!!
            userProfile.occupation = occupation.get()
            userProfile.aboutMe = aboutMe.get()
            userProfile.location = location.get()!!
            userProfile.profileUrl = profileUrl.get()
            if (isCreateProfile.get()) {
                createProfile(userProfile)
            } else {
                mFireStore.collection(Constants.USER).document(userId)
                    .update(mapOf(
                        "aboutMe" to aboutMe.get(),
                        "birthday" to birthday.get(),
                        "displayName" to displayName.get(),
                        "ethnicity" to ethnicity.get(),
                        "figure" to figure.get(),
                        "gender" to gender.get(),
                        "location" to location.get(),
                        "maritalStatus" to maritalStatus.get(),
                        "occupation" to occupation.get(),
                        "realName" to realName.get(),
                        "religion" to religion.get(),
                        "profileUrl" to profileUrl.get()
                    ))
                triggerEvent(EventIdentifier.PROFILE_SUCCESS)
            }
        }
    }

    /**
     * Function to load profile data
     * @author slakra
     * @date 09/07/19
     */
    fun getProfileDetails() {
        mFireStore.collection(Constants.USER).document(userId)
            .get()
            .addOnSuccessListener { documentSnapshot ->
                if (documentSnapshot != null) {
                    val userProfile = documentSnapshot.toObject(UserProfile::class.java)
                    displayName.set(userProfile?.displayName)
                    realName.set(userProfile?.realName)
                    birthday.set(userProfile?.birthday)
                    gender.set(userProfile?.gender)
                    ethnicity.set(userProfile?.ethnicity)
                    religion.set(userProfile?.religion)
                    height.set(userProfile?.height)
                    figure.set(userProfile?.figure)
                    maritalStatus.set(userProfile?.maritalStatus)
                    occupation.set(userProfile?.occupation)
                    aboutMe.set(userProfile?.aboutMe)
                    location.set(userProfile?.location)
                    profileUrl.set(userProfile?.profileUrl)
                }
            }.addOnFailureListener { _ ->
                triggerEvent(EventIdentifier.PROFILE_FETCH_ERROR)
            }
    }

    /**
     * Function to create Profile
     * @param [userProfile] UserProfile
     * @author slakra
     * @date 09/07/19
     */
    private fun createProfile(userProfile: UserProfile) {
        if (isOnline) {
            waitingDialogMsg.value = "Creating New profile"
            mFireStore.collection(Constants.USER).add(userProfile)
                .addOnSuccessListener { doc ->
                    waitingDialogMsg.value = null
                    PreferenceUtil.setUserId(doc.id)
                    triggerEvent(EventIdentifier.PROFILE_SUCCESS)
                }.addOnFailureListener { e ->
                    waitingDialogMsg.value = null
                    triggerEvent(EventIdentifier.PROFILE_FAILURE)
                }
        } else {
            triggerEvent(EventIdentifier.PROFILE_OFFLINE)
        }
    }

    /**
     * Function to upload profile image
     * @author slakra
     * @date 09/08/19
     */
    fun uploadProfileImage() {
        val file = Uri.fromFile(File(profileUrl.get()))
        val metadata = StorageMetadata.Builder()
            .setContentType("image/jpg")
            .build()
        val imageRef = mStorageReference.child("images/$userId.jpg")
        val uploadTask = imageRef.putFile(file, metadata)
        uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            return@Continuation imageRef.downloadUrl
        }).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val downloadUri = task.result
                profileUrl.set(downloadUri?.toString())
                updateProfileUrlPath()
            }
        }
    }

    private fun updateProfileUrlPath() {
        mFireStore.collection(Constants.USER).document(userId)
            .update(mapOf(
                "profileUrl" to profileUrl.get()
            ))
    }

    /**
     * Function to handle profile Picture click
     * @author slakra
     * @date 09/07/19
     */
    fun selectPicture() {
        triggerEvent(EventIdentifier.SELECT_PICTURE)
    }

    /**
     * Function to get City Data
     * @return [LiveData<List<City>> ]
     * @author slakra
     * @date 09/07/19
     */
    fun getCities(): LiveData<List<City>> = cityRepository.getCityList()

    /**
     * Function to get cities from Network
     * @author slakra
     * @date 09/07/19
     */
    @ExperimentalCoroutinesApi
    fun getCityListFromNetwork() {
        CoroutineScope(Dispatchers.IO).launch {
            cityRepository.getCities()
        }
    }

    /**
     * Function to get Gender Single Choice Data
     * @return [LiveData<List<String>>]
     * @author slakra
     * @date 09/07/19
     */
    fun getGenderList(): LiveData<List<String>> = choiceRepository.getChoiceList(Constants.GENDER)

    /**
     * Function to get Ethnicity Single Choice Data
     * @return [LiveData<List<String>>]
     * @author slakra
     * @date 09/07/19
     */
    fun getEthnicityList(): LiveData<List<String>> = choiceRepository.getChoiceList(Constants.ETHNICITY)

    /**
     * Function to get Figure Single Choice Data
     * @return [LiveData<List<String>>]
     * @author slakra
     * @date 09/07/19
     */
    fun getFigureList(): LiveData<List<String>> = choiceRepository.getChoiceList(Constants.FIGURE)

    /**
     * Function to get Religion Single Choice Data
     * @return [LiveData<List<String>>]
     * @author slakra
     * @date 09/07/19
     */
    fun getReligionList(): LiveData<List<String>> = choiceRepository.getChoiceList(Constants.RELIGION)

    /**
     * Function to get Marital Status Single Choice Data
     * @return [LiveData<List<String>>]
     * @author slakra
     * @date 09/07/19
     */
    fun getMaritalStatusList(): LiveData<List<String>> =
        choiceRepository.getChoiceList(Constants.MARITAL_STATUS)

    /**
     * Function to get Single Choice Data
     * @author slakra
     * @date 09/07/19
     */
    @ExperimentalCoroutinesApi
    fun getSingleChoices() {
        CoroutineScope(Dispatchers.IO).launch {
            choiceRepository.clearPreviousChoices()
            choiceRepository.getChoices()
        }
    }

    /**
     * Function to handle birthday click
     * @author slarka
     * @date 09/07/19
     *
     */
    fun showBirthdayPicker() = triggerEvent(EventIdentifier.SHOW_BIRTHDAY_PICKER)

    private fun validateFields() = when {
            TextUtils.isEmpty(displayName.get()) -> {
                errorDisplayName.set(R.string.empty_error)
                false
            }
            TextUtils.isEmpty(realName.get()) -> {
                errorRealName.set(R.string.empty_error)
                false
            }
            TextUtils.isEmpty(birthday.get()) -> {
                errorBirthday.set(R.string.empty_error)
                false
            }
            TextUtils.isEmpty(location.get()) -> {
                errorLocation.set(R.string.empty_error)
                false
            }
            else -> true
        }

    private fun resetError() {
        errorDisplayName.set(null)
        errorRealName.set(null)
        errorBirthday.set(null)
        errorLocation.set(null)
    }
}
