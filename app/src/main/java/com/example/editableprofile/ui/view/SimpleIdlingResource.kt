package com.example.editableprofile.ui.view

import androidx.test.espresso.IdlingResource

/**
 * IdlingResource class to help in UI Testing
 * @author slakra
 * @date 09/12/19
 */
class SimpleIdlingResource(private var name: String) : IdlingResource {
    private var resourceCallback: IdlingResource.ResourceCallback? = null
    private var counter: Int = 0

    override fun getName() = name

    override fun isIdleNow() = counter <= 0

    @Suppress("UnsafeCallOnNullableType")
    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback?) {
        this.resourceCallback = callback!!
    }

    /**
     * Function to increment count
     * @author slakra
     * @date 09/12/19
     */
    fun incrementCounter() = counter++

    /**
     * Function to increment count
     * @author slakra
     * @date 09/11/19
     */
    fun decrementCounter() {
        counter--

        if (counter == 0) {
            resourceCallback?.onTransitionToIdle()
        }
    }
}
