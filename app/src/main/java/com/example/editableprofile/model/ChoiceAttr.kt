package com.example.editableprofile.model

/**
 * @brief: Single ChoiceAttr data class
 * @author Sumit Lakra
 * @date 09/02/2019
 */
data class ChoiceAttr(
    var id: String = "",
    var name: String = ""
)