package com.example.editableprofile.model

/**
 * Model Class to handle User Profile data
 * @author slakra
 * @date 09/04/19
 */
data class UserProfile(
    var displayName: String = "",
    var realName: String = "",
    var profileUrl: String? = "",
    var birthday: String = "",
    var gender: String = "",
    var ethnicity: String? = "",
    var religion: String? = "",
    var height: String? = "",
    var figure: String? = "",
    var maritalStatus: String = "",
    var occupation: String? = "",
    var aboutMe: String? = "",
    var location: String = ""
)