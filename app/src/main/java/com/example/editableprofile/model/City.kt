package com.example.editableprofile.model

/**
 * @brief: City data class
 * @author Sumit Lakra
 * @date 09/02/2019
 */
data class City(
    var lat: String = "",
    var lon: String = "",
    var city: String = ""
)