package com.example.editableprofile.model.response

import com.example.editableprofile.model.ChoiceAttr

/**
 * @brief: Response class to handle getSingleChoice Request
 * @author slakra
 * @date 09/02/19
 */
data class SingleChoiceResponse(
    var gender: List<ChoiceAttr>,
    var ethnicity: List<ChoiceAttr>,
    var religion: List<ChoiceAttr>,
    var figure: List<ChoiceAttr>,
    var maritalStatus: List<ChoiceAttr>
)