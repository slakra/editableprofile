package com.example.editableprofile.model

/**
 * @brief: enum class to hold event types
 * @author: Sumit Lakra
 * @date 09/02/2019
 */
enum class EventIdentifier {
    EDIT_PROFILE_CLICKED,
    CREATE_PROFILE_CLICKED,
    SAVE_CHANGES,
    SHOW_BIRTHDAY_PICKER,
    PROFILE_SUCCESS,
    PROFILE_FAILURE,
    PROFILE_OFFLINE,
    PROFILE_FETCH_ERROR,
    SELECT_PICTURE
}