package com.example.editableprofile.model.response

import com.example.editableprofile.model.City

/**
 * @brief: Response class to handle getCities Request
 * @author slakra
 * @date 09/02/19
 */
data class CityResponse(
    var cities: List<City>
)