package com.example.editableprofile.common.util

import com.orhanobut.hawk.Hawk

/**
 * Utility class to manage Shared preferences
 * @author slakra
 * @date 09/08/19
 */
object PreferenceUtil {

    private const val KEY_USER_ID = "user_id"

    /**
     * Function to set userId in Preference
     * @param [userId] String
     * @author slakra
     * @date 09/08/19
     */
    fun setUserId(userId: String) {
        Hawk.put(KEY_USER_ID, userId)
    }

    /**
     * Function to delete all data from Preference
     * @param [userId] String
     * @author slakra
     * @date 09/12/19
     */
    fun clearPreference() {
        Hawk.deleteAll()
    }

    val userId: String?
        get() = Hawk.get(KEY_USER_ID)
}
