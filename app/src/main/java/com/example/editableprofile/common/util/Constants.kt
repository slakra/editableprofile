package com.example.editableprofile.common.util

/**
 * Class to hold application constants
 * @author slakra
 * @date 09/02/19
 */
object Constants {

    const val TIME_OUT = 10L
    const val PROFILE = "PROFILE"
    const val IS_CREATE_PROFILE = "is_create_profile"
    const val GENDER = "GENDER"
    const val ETHNICITY = "ETHNICTY"
    const val RELIGION = "RELIGION"
    const val FIGURE = "FIGURE"
    const val MARITAL_STATUS = "MARITAL_STATUS"
    const val USER = "USER"
    const val PICK_IMAGE = 100
    const val READ_STORAGE = 200
}
