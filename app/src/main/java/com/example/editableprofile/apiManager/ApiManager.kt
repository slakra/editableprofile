package com.example.editableprofile.apiManager

import android.util.Log
import com.example.editableprofile.common.util.Constants
import com.example.editableprofile.database.dao.ChoiceDao
import com.example.editableprofile.database.dao.CityDao
import com.example.editableprofile.database.entity.ChoiceEntity
import com.example.editableprofile.database.entity.CityEntity
import com.example.editableprofile.database.helper.toDBEntity
import com.example.editableprofile.model.response.CityResponse
import com.example.editableprofile.model.response.SingleChoiceResponse
import com.example.editableprofile.network.IApiService
import java.lang.Exception
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.ExperimentalCoroutinesApi

/**
 * @brief: Manager class to handle Api calls
 * @author: Slakra
 * @date: 09/03/19
 */
@Suppress("TooGenericExceptionCaught")
open class ApiManager(val apiService: IApiService, val cityDao: CityDao, val choiceDao: ChoiceDao) {

    private val mTAG = ApiManager::class.java.simpleName

    /**
     * Function to get city list from server
     * @return [Boolean]
     * @author slakra
     * @date 09/03/19
     */
    @ExperimentalCoroutinesApi
    open suspend fun getCities(): Boolean {
        val request = apiService.getCities()
        val response = executeCityCall(request)
        if (response is CityResponse) {
            val cityEntityList = ArrayList<CityEntity>()
            for (city in response.cities) {
                cityEntityList.add(city.toDBEntity())
            }
            cityDao.insertAll(cityEntityList)
            return true
        }
        return false
    }

    /**
     * Function to get Single choice from server
     * @return [Boolean]
     * @author slakra
     * @date 09/03/19
     */
    @ExperimentalCoroutinesApi
    open suspend fun getSingleChoices(): Boolean {
        val request = apiService.getSingleChoices()
        val response = executeChoiceCall(request)
        if (response is SingleChoiceResponse) {
            val genderList = response.gender
            val entityList = ArrayList<ChoiceEntity>()
            for (genderItem in genderList) {
                entityList.add(genderItem.toDBEntity(Constants.GENDER))
            }

            val ethnicityList = response.ethnicity
            for (ethItem in ethnicityList) {
                entityList.add(ethItem.toDBEntity(Constants.ETHNICITY))
            }

            val figureList = response.figure
            for (figureItem in figureList) {
                entityList.add(figureItem.toDBEntity(Constants.FIGURE))
            }

            val religionList = response.religion
            for (religionItem in religionList) {
                entityList.add(religionItem.toDBEntity(Constants.RELIGION))
            }

            val maritalStatusList = response.maritalStatus
            for (statusItem in maritalStatusList) {
                entityList.add(statusItem.toDBEntity(Constants.MARITAL_STATUS))
            }
            choiceDao.insertAll(entityList)
            return true
        }
        return false
    }

    @ExperimentalCoroutinesApi
    private suspend fun executeCityCall(call: Deferred<CityResponse>) = try {
        call.join()
        call.getCompleted()
    } catch (ex: Exception) {
        Log.e(mTAG, "Exception during network operation -> ${ex.printStackTrace()}")
    }

    @ExperimentalCoroutinesApi
    private suspend fun executeChoiceCall(call: Deferred<SingleChoiceResponse>) = try {
        call.join()
        call.getCompleted()
    } catch (ex: Exception) {
        Log.e(mTAG, "Exception during network operation -> ${ex.printStackTrace()}")
    }
}
