package com.example.editableprofile.di

/**
 * Will contain list of all kotlin-koin modules
 * @author Sumit Lakra
 * @date 09/02/19
 */
val appModule = listOf(commonModule, databaseModule, repositoryModule, viewModelModule, networkModule, managerModule)