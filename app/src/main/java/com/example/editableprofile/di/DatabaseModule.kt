package com.example.editableprofile.di

import androidx.room.Room
import com.example.editableprofile.database.EditProfileDataBase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module

/**
 * Module will initialize database and all dao objects
 * @author Sumit Lakra
 * @date 09/02/19
 */
internal val databaseModule = module {
    // Room Database
    single {
        Room.databaseBuilder(androidApplication(), EditProfileDataBase::class.java, "editProfile-db")
            .allowMainThreadQueries().build()
    }

    // Expose city dao
    single { get<EditProfileDataBase>().cityDao() }

    // Expose choice dao
    single { get<EditProfileDataBase>().choiceDao() }
}