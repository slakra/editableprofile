package com.example.editableprofile.di

import com.example.editableprofile.components.IRouter
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import org.koin.dsl.module.module

/**
@file_name: CommonModule.kt
@author: Sumit
@brief: CommonModule class to hold all DI objects which are kind of utilities.
@date: 09/02/2019
 */
internal val commonModule = module {

    @Suppress("UNCHECKED_CAST")
    single { Router() as IRouter }

    single { FirebaseFirestore.getInstance() }

    single { FirebaseStorage.getInstance("gs://editableprofile.appspot.com").reference }
}