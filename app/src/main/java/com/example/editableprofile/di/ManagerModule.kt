package com.example.editableprofile.di

import com.example.editableprofile.apiManager.ApiManager
import org.koin.dsl.module.module

internal val managerModule = module {

    single { ApiManager(apiService = get(), choiceDao = get(), cityDao = get()) }
}