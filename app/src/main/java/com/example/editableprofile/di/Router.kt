package com.example.editableprofile.di

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.editableprofile.common.ViewType
import com.example.editableprofile.components.BaseActivity
import com.example.editableprofile.components.IRouter
import com.example.editableprofile.ui.view.EditProfile
import com.example.editableprofile.ui.view.ViewProfileActivity

/**
@file_name: Router.kt
@author: Sumit Lakra
@brief: Router class to manage app navigation.
@date: 09/02/2019
 */
class Router : IRouter {

    /**
     * Navigate to particular activity with startforresult
     * @param caller BaseActivity
     * @param destinationType Int
     * @param bundle Bundle
     * @param requestCode Int
     * @author Sumit Lakra
     * @date: 09/02/2019
     */
    @Suppress("EmptyFunctionBlock")
    override fun routeForResult(caller: BaseActivity<*, *>, destinationType: Int, bundle: Bundle, requestCode: Int) {
    }

    /**
     * Navigate to particular activity
     * @param caller BaseActivity
     * @param destinationType Int
     * @param bundle Bundle
     * @param requestCode Int
     * @author Sumit Lakra
     * @date: 09/02/2019
     */
    override fun routeTo(caller: BaseActivity<*, *>, destinationType: Int, bundle: Bundle) {
        when (destinationType) {
            ViewType.EDIT_PROFILE -> caller.startActivity(getIntent(caller, EditProfile::class.java, bundle))
        }
    }

    /**
     * Navigate to particular activity
     * @param caller Context
     * @param destinationType Int
     * @param bundle Bundle
     * @param requestCode Int
     * @author Sumit Lakra
     * @date: 09/02/2019
     */
    @Suppress("EmptyFunctionBlock")
    override fun routeTo(caller: Context, destinationType: Int, bundle: Bundle) {
    }

    /**
     * called when navigation is needed from one to another activity with flag for intent
     * @param [caller] BaseActivity
     * @param [destinationType] Int
     * @param [bundle] Bundle
     * @param [flag] Int
     * @author Sumit Lakra
     * @date: 09/02/2019
     */
    override fun routeToWithFlag(
        caller: BaseActivity<*, *>,
        destinationType: Int,
        bundle: Bundle,
        flag: Int
    ) {
        when (destinationType) {
            ViewType.VIEW_PROFILE -> caller.startActivity(
                getIntent(caller, ViewProfileActivity::class.java, bundle, flag))
        }
    }

    /**
     * generate intent based on given inputs.
     * @param caller BaseActivity
     * @param destination Class<T>
     * @param bundle Bundle
     * @return Intent
     * @author Sumit Lakra
     * @date: 09/02/2019
     */
    private fun <T> getIntent(caller: Context, destination: Class<T>, bundle: Bundle): Intent {
        val intent = Intent(caller, destination)
        intent.putExtras(bundle)
        return intent
    }

    /**
     * generate intent based on given inputs.
     * @param caller BaseActivity
     * @param destination Class<T>
     * @param bundle Bundle
     * @return Intent
     * @author Sumit Lakra
     * @date: 09/02/2019
     */
    private fun <T> getIntent(caller: Context, destination: Class<T>, bundle: Bundle, flag: Int): Intent {
        val intent = Intent(caller, destination)
        intent.putExtras(bundle)
        intent.flags = flag
        return intent
    }
}