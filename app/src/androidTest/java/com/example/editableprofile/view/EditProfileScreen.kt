package com.example.editableprofile.view

import android.widget.ListView
import androidx.test.espresso.DataInteraction
import com.agoda.kakao.edit.KEditText
import com.agoda.kakao.list.KAbsListView
import com.agoda.kakao.list.KAdapterItem
import com.agoda.kakao.picker.date.KDatePickerDialog
import com.agoda.kakao.screen.Screen
import com.agoda.kakao.scroll.KScrollView
import com.agoda.kakao.text.KButton
import com.agoda.kakao.text.KTextView
import com.example.editableprofile.R

open class EditProfileScreen : Screen<EditProfileScreen>() {

    val scrollView = KScrollView { withId(R.id.scroll_view) }
    val displayName = KEditText { withId(R.id.et_display_name) }
    val realName = KEditText { withId(R.id.et_real_name) }
    val birthday = KEditText { withId(R.id.et_birthday) }
    val height = KEditText { withId(R.id.et_height) }
    val occupation = KEditText { withId(R.id.et_occupation) }
    val aboutMe = KEditText { withId(R.id.et_about_me) }
    val editProfile = KButton { withId(R.id.btn_edit_profile) }
    val autoComplete = KEditText { withId(R.id.location_actv) }
    val cityList = KAbsListView(
        builder = { isInstanceOf(ListView::class.java) },
        itemTypeBuilder = { itemType(::Item) })

    class Item(i: DataInteraction) : KAdapterItem<Item>(i) {
        val text = KTextView(i) { withId(android.R.id.text1) }
    }

    val datePickerDialog: KDatePickerDialog = KDatePickerDialog()
}