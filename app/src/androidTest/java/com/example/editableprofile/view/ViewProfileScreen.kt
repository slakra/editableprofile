package com.example.editableprofile.view

import com.agoda.kakao.image.KImageView
import com.agoda.kakao.screen.Screen
import com.agoda.kakao.text.KButton
import com.agoda.kakao.text.KTextView
import com.example.editableprofile.R

open class ViewProfileScreen : Screen<ViewProfileScreen>() {

    val profilePic = KImageView { withId(R.id.iv_profile_pic) }
    val displayName = KTextView { withId(R.id.tv_display_name) }
    val birthDay = KTextView { withId(R.id.tv_birthday) }
    val gender = KTextView { withId(R.id.tv_gender) }
    val ethnicity = KTextView { withId(R.id.tv_ethnicity) }
    val religion = KTextView { withId(R.id.tv_religion) }
    val height = KTextView { withId(R.id.tv_height) }
    val figure = KTextView { withId(R.id.tv_figure) }
    val aboutMe = KTextView { withId(R.id.tv_about_me) }
    val location = KTextView { withId(R.id.tv_location) }

    val editProfile = KButton { withId(R.id.btn_edit_profile) }

    val message = KTextView { withId(R.id.tv_message) }
    val createProfile = KButton { withId(R.id.btn_create_profile) }
}