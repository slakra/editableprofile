package com.example.editableprofile.view

import android.content.Intent
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.UiDevice
import com.agoda.kakao.intent.KIntent
import com.example.editableprofile.R
import com.example.editableprofile.common.util.PreferenceUtil
import com.example.editableprofile.ui.view.EditProfile
import com.example.editableprofile.ui.view.EspressoIdlingResource
import com.example.editableprofile.ui.view.ViewProfileActivity
import com.example.editableprofile.utils.TestConstant
import kotlin.system.measureTimeMillis
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
@LargeTest
class ViewProfileActivityTest {

    @get:Rule
    var activityScenarioRule = ActivityTestRule(ViewProfileActivity::class.java, false, false)

    @get:Rule
    var intentsRule = IntentsTestRule(ViewProfileActivity::class.java)

    private val viewProfileScreen = ViewProfileScreen()

    private val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

    @Before
    fun setUp() {
        IdlingRegistry.getInstance().register(EspressoIdlingResource.getIdlingResource())
        PreferenceUtil.clearPreference()
    }

    @Test
    fun initialViewsTest() {
        val timeElapsed = measureTimeMillis {
            activityScenarioRule.launchActivity(Intent())
        }
        device.waitForIdle(4000)
        viewProfileScreen {
            createProfile { isDisplayed() }
            message {
                isDisplayed()
                hasText(R.string.profile_data_not_found)
            }
        }

        Assert.assertTrue(timeElapsed <= TestConstant.MAX_ACTIVITY_STARTUP_TIME)
    }

    @Test
    fun createProfileClickTest() {
        activityScenarioRule.launchActivity(Intent())

        device.waitForIdle(6000)

        viewProfileScreen {
            createProfile.click()

            val createProfileIntent = KIntent {
                hasComponent(EditProfile::class.java.name)
                hasExtra(TestConstant.IS_CREATE_PROFILE, true)
            }
            createProfileIntent.intended()
        }
    }

    @Test
    fun editProfileClickTest() {
        PreferenceUtil.setUserId("Dp0PHQhHZBxrszAJe7lQ")
        activityScenarioRule.launchActivity(Intent())
        device.waitForIdle(6000)

        viewProfileScreen {
            editProfile {
                isVisible()
                isDisplayed()
                click()
            }
            val editProfileIntent = KIntent {
                hasComponent(EditProfile::class.java.name)
                hasExtra(TestConstant.IS_CREATE_PROFILE, false)
            }
            editProfileIntent.intended()
        }
    }

    @Test
    fun viewProfileTest() {
        PreferenceUtil.setUserId("Dp0PHQhHZBxrszAJe7lQ")
        activityScenarioRule.launchActivity(Intent())
        device.waitForIdle(6000)

        viewProfileScreen {
            profilePic {
                isDisplayed()
            }
            displayName {
                isDisplayed()
                hasText("sam")
            }
            birthDay {
                isDisplayed()
                hasText("18/2/1992")
            }
            ethnicity {
                isDisplayed()
                hasText("test1")
            }
            figure {
                isDisplayed()
                hasText("test1")
            }
            gender {
                isDisplayed()
                hasText("test1")
            }
            height {
                isDisplayed()
                hasText("172 cm")
            }
            religion {
                hasText("test1")
            }
            location {
                hasText("56°09'N, 10°13'E")
            }
            editProfile {
                isDisplayed()
                hasText(R.string.edit_profile)
            }

            createProfile {
                isGone()
            }
            message {
                isGone()
            }
        }
    }

    @After
    fun tearUp() {
        PreferenceUtil.clearPreference()
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.getIdlingResource())
    }
}
