package com.example.editableprofile.view

import android.content.Intent
import androidx.test.espresso.IdlingRegistry
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.UiDevice
import com.example.editableprofile.R
import com.example.editableprofile.common.util.PreferenceUtil
import com.example.editableprofile.database.dao.ChoiceDao
import com.example.editableprofile.database.dao.CityDao
import com.example.editableprofile.database.entity.ChoiceEntity
import com.example.editableprofile.database.entity.CityEntity
import com.example.editableprofile.ui.view.EditProfile
import com.example.editableprofile.ui.view.EspressoIdlingResource
import com.example.editableprofile.utils.TestConstant
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.standalone.get
import org.koin.test.KoinTest

@RunWith(AndroidJUnit4ClassRunner::class)
@LargeTest
class EditProfileActivityTestTwo : KoinTest {

    private val cityDao: CityDao = get()
    private val choiceDao: ChoiceDao = get()

    @get:Rule
    val activityRule = ActivityTestRule(EditProfile::class.java, false, false)

    val editProfileScreen = EditProfileScreen()

    private val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

    @Before
    fun setUp() {
        IdlingRegistry.getInstance().register(EspressoIdlingResource.getIdlingResource())
        insertDataInDb()
        PreferenceUtil.setUserId("Dp0PHQhHZBxrszAJe7lQ")
    }

    @Test
    fun editProfile() {
        launchEditProfileActivity()
        device.waitForIdle(6000)

        editProfileScreen {
            realName {
                isDisplayed()
                clearText()
                typeText("Sumit lakra")
            }
            device.waitForIdle()
            scrollView {
                scrollToEnd()
            }
            device.waitForIdle()
            occupation {
                isDisplayed()
                clearText()
                typeText("test")
            }
            editProfile {
                isDisplayed()
                click()
            }
            device.waitForIdle()
        }
    }

    @Test
    fun initialValueTest() {
        launchEditProfileActivity()
        device.waitForIdle(6000)

        editProfileScreen {
            realName {
                isDisplayed()
                hasText("Sumit lakra")
            }
            displayName {
                isDisplayed()
                hasText("sam")
            }
            height {
                isDisabled()
                hasText("172 cm")
            }
            scrollView {
                scrollToEnd()
            }
            device.waitForIdle()
            autoComplete {
                isDisplayed()
                hasText("56°09'N, 10°13'E")
            }
            editProfile {
                isDisplayed()
                hasText(R.string.save_profile)
            }
        }
    }

    @Test
    fun birthdayPickerTest() {
        val activity = launchEditProfileActivity()
        device.waitForIdle(6000)

        editProfileScreen {
            birthday {
                click()
            }
            device.waitForIdle()

            datePickerDialog {
                datePicker {
                    setDate(2019, 8, 14)
                    hasDate(2019, 8, 14)
                }
                cancelButton {
                    click()
                }
            }

            birthday {
                click()
            }
            device.waitForIdle()

            datePickerDialog {
                datePicker {
                    setDate(1992, 10, 12)
                }
                okButton {
                    click()
                }
            }
            device.waitForIdle()

            assert(activity.mViewModel.birthday.get() == "18/10/1992")
        }
    }

    @After
    fun tearUp() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.getIdlingResource())
        cityDao.deleteAllCities()
        choiceDao.deleteAllChoices()
    }

    private fun launchEditProfileActivity(): EditProfile {
        val intent = Intent()
        intent.putExtra(TestConstant.IS_CREATE_PROFILE, false)
        return activityRule.launchActivity(intent)
    }

    private fun insertDataInDb() {
        val cityEntity = CityEntity()
        cityEntity.city = "delhi"
        cityEntity.lat = "123"
        cityEntity.lon = "234"
        cityDao.insertEntity(cityEntity)
        cityEntity.city = "haryana"
        cityDao.insertEntity(cityEntity)

        val choiceEntity = ChoiceEntity()
        choiceEntity.name = "test1"
        choiceEntity.category = TestConstant.ETHNICITY
        choiceDao.insertEntity(choiceEntity)
        choiceEntity.name = "test2"
        choiceDao.insertEntity(choiceEntity)

        choiceEntity.name = "test1"
        choiceEntity.category = TestConstant.GENDER
        choiceDao.insertEntity(choiceEntity)
        choiceEntity.name = "test2"
        choiceDao.insertEntity(choiceEntity)

        choiceEntity.name = "test1"
        choiceEntity.category = TestConstant.FIGURE
        choiceDao.insertEntity(choiceEntity)
        choiceEntity.name = "test2"
        choiceDao.insertEntity(choiceEntity)

        choiceEntity.name = "test1"
        choiceEntity.category = TestConstant.MARITAL_STATUS
        choiceDao.insertEntity(choiceEntity)
        choiceEntity.name = "test2"
        choiceDao.insertEntity(choiceEntity)

        choiceEntity.name = "test1"
        choiceEntity.category = TestConstant.RELIGION
        choiceDao.insertEntity(choiceEntity)
        choiceEntity.name = "test2"
        choiceDao.insertEntity(choiceEntity)
    }
}