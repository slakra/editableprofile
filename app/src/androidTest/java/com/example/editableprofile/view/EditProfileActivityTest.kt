package com.example.editableprofile.view

import android.content.Intent
import androidx.test.espresso.IdlingRegistry
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.UiDevice
import com.example.editableprofile.R
import com.example.editableprofile.database.dao.ChoiceDao
import com.example.editableprofile.database.dao.CityDao
import com.example.editableprofile.database.entity.ChoiceEntity
import com.example.editableprofile.database.entity.CityEntity
import com.example.editableprofile.ui.view.EditProfile
import com.example.editableprofile.ui.view.EspressoIdlingResource
import com.example.editableprofile.utils.TestConstant
import com.example.editableprofile.utils.TestUtil
import com.squareup.okhttp.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.standalone.get
import org.koin.test.KoinTest

@RunWith(AndroidJUnit4ClassRunner::class)
@LargeTest
class EditProfileActivityTest : KoinTest {
    private val cityDao: CityDao = get()
    private val choiceDao: ChoiceDao = get()

    @get:Rule
    val activityRule = ActivityTestRule(EditProfile::class.java, false, false)

    val editProfileScreen = EditProfileScreen()

    private lateinit var mockWebServer: MockWebServer

    @Before
    fun setUp() {
        IdlingRegistry.getInstance().register(EspressoIdlingResource.getIdlingResource())
        insertDataInDb()
        mockWebServer = MockWebServer()
        mockWebServer.play(TestUtil.MOCK_PORT)
    }

    private val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

    @Test
    fun createProfileInitialViewTest() {
        launchCreateProfileActivity()
        device.waitForIdle()
        editProfileScreen {
            displayName {
                isDisplayed()
                hasText("")
            }
            realName {
                isDisplayed()
                hasText("")
            }
            height {
                isDisplayed()
                hasText("")
            }
            scrollView {
                scrollToEnd()
            }
            device.waitForIdle()
            occupation {
                isDisplayed()
                hasText("")
            }
            aboutMe {
                isDisplayed()
                hasText("")
            }
            editProfile {
                isDisplayed()
                hasText(R.string.create_profile)
            }
        }
    }

    @Test
    fun autoCompleteTest() {
        mockWebServer.enqueue(TestUtil.createResponse("cities.json"))
        mockWebServer.enqueue(TestUtil.createResponse("single_choice_attributes.json"))
        launchCreateProfileActivity()
        device.waitForIdle()
        editProfileScreen {
            scrollView {
                scrollToEnd()
            }
            device.waitForIdle()

            autoComplete {
                isDisplayed()
                typeText("delhi")
            }
            device.waitForIdle()

            cityList {
                inRoot {
                    isPlatformPopup()
                }
                isVisible()
                hasSize(1)
            }
        }
    }

    @After
    fun tearUp() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.getIdlingResource())
        mockWebServer.shutdown()
        cityDao.deleteAllCities()
        choiceDao.deleteAllChoices()
    }

    private fun launchCreateProfileActivity(): EditProfile {
        val intent = Intent()
        intent.putExtra(TestConstant.IS_CREATE_PROFILE, true)
        return activityRule.launchActivity(intent)
    }

    private fun insertDataInDb() {
        val cityEntity = CityEntity()
        cityEntity.city = "delhi"
        cityEntity.lat = "123"
        cityEntity.lon = "234"
        cityDao.insertEntity(cityEntity)
        cityEntity.city = "haryana"
        cityDao.insertEntity(cityEntity)

        val choiceEntity = ChoiceEntity()
        choiceEntity.name = "test1"
        choiceEntity.category = TestConstant.ETHNICITY
        choiceDao.insertEntity(choiceEntity)
        choiceEntity.name = "test2"
        choiceDao.insertEntity(choiceEntity)

        choiceEntity.name = "test1"
        choiceEntity.category = TestConstant.GENDER
        choiceDao.insertEntity(choiceEntity)
        choiceEntity.name = "test2"
        choiceDao.insertEntity(choiceEntity)

        choiceEntity.name = "test1"
        choiceEntity.category = TestConstant.FIGURE
        choiceDao.insertEntity(choiceEntity)
        choiceEntity.name = "test2"
        choiceDao.insertEntity(choiceEntity)

        choiceEntity.name = "test1"
        choiceEntity.category = TestConstant.MARITAL_STATUS
        choiceDao.insertEntity(choiceEntity)
        choiceEntity.name = "test2"
        choiceDao.insertEntity(choiceEntity)

        choiceEntity.name = "test1"
        choiceEntity.category = TestConstant.RELIGION
        choiceDao.insertEntity(choiceEntity)
        choiceEntity.name = "test2"
        choiceDao.insertEntity(choiceEntity)
    }
}
