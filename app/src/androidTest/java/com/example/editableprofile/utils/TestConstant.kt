package com.example.editableprofile.utils

/**
 * Class to hold Test constants
 * @author slakra
 * @date 09/13/19
 */
object TestConstant {
    const val MAX_ACTIVITY_STARTUP_TIME = 2000
    const val IS_CREATE_PROFILE = "is_create_profile"
    const val GENDER = "GENDER"
    const val ETHNICITY = "ETHNICTY"
    const val RELIGION = "RELIGION"
    const val FIGURE = "FIGURE"
    const val MARITAL_STATUS = "MARITAL_STATUS"
}