package com.example.editableprofile.utils

import androidx.test.platform.app.InstrumentationRegistry
import com.squareup.okhttp.mockwebserver.MockResponse

/**
 * Test Util class
 * @author slakra
 * @date 09/12/19
 */
object TestUtil {

    const val MOCK_PORT: Int = 8888

    private fun loadJson(path: String): String {
        val context = InstrumentationRegistry.getInstrumentation().context
        val stream = context.resources.assets.open(path)
        val reader = stream.bufferedReader()
        val stringBuilder = StringBuilder()
        reader.lines().forEach {
            stringBuilder.append(it)
        }
        return stringBuilder.toString()
    }

    /**
     * Function to create MockResponse
     * @param [path] String
     * @return [MockResponse]
     * @author Slakra
     * @date 09/12/19
     */
    fun createResponse(path: String): MockResponse = MockResponse()
        .setResponseCode(200)
        .setBody(loadJson(path))
}