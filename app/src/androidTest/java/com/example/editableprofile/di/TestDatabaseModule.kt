package com.example.editableprofile.di

import androidx.room.Room
import com.example.editableprofile.database.EditProfileDataBase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module

/**
 * Test Module will initialize database and all dao objects
 * @author Sumit Lakra
 * @date 09/13/19
 */
internal val testDatabaseModule = module {

    single { Room.inMemoryDatabaseBuilder(androidApplication(),
        EditProfileDataBase::class.java)
        .allowMainThreadQueries().build()
    }

    single { get<EditProfileDataBase>().cityDao() }

    single { get<EditProfileDataBase>().choiceDao() }
}