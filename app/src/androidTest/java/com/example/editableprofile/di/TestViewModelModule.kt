package com.example.editableprofile.di

import com.example.editableprofile.ui.viewModel.EditProfileViewModel
import com.example.editableprofile.ui.viewModel.ViewProfileViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

/**
 * Module will initialize all viewmodels
 * @author Sumit Lakra
 * @date 09/13/19
 */
internal val testViewModelModule = module {
    // initialize all viewmodule here
    viewModel { ViewProfileViewModel(get()) }
    viewModel { EditProfileViewModel(get(), get(), get(), get()) }
}