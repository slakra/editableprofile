package com.example.editableprofile.di

import com.example.editableprofile.database.repository.ChoiceRepository
import com.example.editableprofile.database.repository.CityRepository
import org.koin.dsl.module.module

/**
 * Test Module will initialize all repositories
 * @author Sumit Lakra
 * @date 09/13/19
 */
internal val testRepositoryModule = module {

    single { CityRepository(cityDao = get(), apiManager = get()) }

    single { ChoiceRepository(choiceDao = get(), apiManager = get()) }
}