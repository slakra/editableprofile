package com.example.editableprofile.di

import android.app.Application
import com.orhanobut.hawk.Hawk
import org.koin.android.ext.android.startKoin

/**
 * Test application class to initialize test related dependencies
 * @author slarka
 * @date 09/13/19
 */
class TestApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
        initHawk()
    }

    private fun initKoin() {
        startKoin(this, testAppModule)
    }

    private fun initHawk() {
        Hawk.init(this).build()
    }
}