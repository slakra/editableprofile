package com.example.editableprofile.di

/**
 * Test appModule Will contain list of all kotlin-koin modules
 * @author slarka
 * @date 09/13/19
 */
val testAppModule = listOf(commonModule, testDatabaseModule, testRepositoryModule,
    testViewModelModule, networkModule, managerModule)