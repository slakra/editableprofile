package com.example.editableprofile.model

import com.example.editableprofile.model.response.CityResponse
import com.example.editableprofile.model.response.SingleChoiceResponse
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Test

class ModelTest {

    @Test
    fun choiceAttrTest() {
        val choiceAttr = ChoiceAttr()
        choiceAttr.id = "123"
        choiceAttr.name = "name"

        assertTrue(choiceAttr.name == "name")
        assertTrue(choiceAttr.id == "123")
    }

    @Test
    fun cityTest() {
        val city = City()
        city.city = "city"
        city.lat = "123"
        city.lon = "234"

        assertTrue(city.city == "city")
        assertTrue(city.lat == "123")
        assertTrue(city.lon == "234")
    }

    @Test
    fun cityResponseTest() {
        val cities = ArrayList<City>()
        val cityResponse = CityResponse(cities)

        assertNotNull(cityResponse.cities)
    }

    @Test
    fun singleChoiceResponseTest() {
        val mockList = ArrayList<ChoiceAttr>()
        val singleChoiceResponse = SingleChoiceResponse(mockList, mockList, mockList, mockList, mockList)

        assertNotNull(singleChoiceResponse.gender)
        assertNotNull(singleChoiceResponse.ethnicity)
        assertNotNull(singleChoiceResponse.religion)
        assertNotNull(singleChoiceResponse.figure)
        assertNotNull(singleChoiceResponse.maritalStatus)
    }

    @Test
    fun userProfileModelTest() {
        val userProfile = UserProfile("Sam", "Sumit", "TestUrl", "birthday",
            "male", "asian", "hindu", "172", "slim", "married",
            "job", "cool", "delhi")

        assertTrue(userProfile.displayName == "Sam")
        assertTrue(userProfile.realName == "Sumit")
        assertTrue(userProfile.profileUrl == "TestUrl")
        assertTrue(userProfile.birthday == "birthday")
        assertTrue(userProfile.gender == "male")
        assertTrue(userProfile.ethnicity == "asian")
        assertTrue(userProfile.religion == "hindu")
        assertTrue(userProfile.height == "172")
        assertTrue(userProfile.figure == "slim")
        assertTrue(userProfile.maritalStatus == "married")
        assertTrue(userProfile.occupation == "job")
        assertTrue(userProfile.aboutMe == "cool")
        assertTrue(userProfile.location == "delhi")
    }

    @Test
    fun userProfileGetterSetterTest() {
        val userProfile = UserProfile()
        userProfile.displayName = "sam"
        userProfile.realName = "Sumit"
        userProfile.profileUrl = "TestUrl"
        userProfile.birthday = "birthday"
        userProfile.gender = "male"
        userProfile.ethnicity = "asian"
        userProfile.religion = "hindu"
        userProfile.height = "172"
        userProfile.figure = "slim"
        userProfile.maritalStatus = "married"
        userProfile.occupation = "job"
        userProfile.aboutMe = "cool"
        userProfile.location = "delhi"

        assertTrue(userProfile.displayName == "sam")
        assertTrue(userProfile.realName == "Sumit")
        assertTrue(userProfile.profileUrl == "TestUrl")
        assertTrue(userProfile.birthday == "birthday")
        assertTrue(userProfile.gender == "male")
        assertTrue(userProfile.ethnicity == "asian")
        assertTrue(userProfile.religion == "hindu")
        assertTrue(userProfile.height == "172")
        assertTrue(userProfile.figure == "slim")
        assertTrue(userProfile.maritalStatus == "married")
        assertTrue(userProfile.occupation == "job")
        assertTrue(userProfile.aboutMe == "cool")
        assertTrue(userProfile.location == "delhi")
    }
}