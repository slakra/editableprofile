package com.example.editableprofile.viewModel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.editableprofile.common.util.PreferenceUtil
import com.example.editableprofile.model.EventIdentifier
import com.example.editableprofile.model.UserProfile
import com.example.editableprofile.ui.viewModel.ViewProfileViewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.nhaarman.mockito_kotlin.mock
import com.orhanobut.hawk.Hawk
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import org.powermock.modules.junit4.PowerMockRunnerDelegate

@RunWith(PowerMockRunner::class)
@PowerMockRunnerDelegate(MockitoJUnitRunner::class)
@PrepareForTest(PreferenceUtil::class, Hawk::class, FirebaseFirestore::class)
class ViewProfileModelTest {

    @Before
    fun setUp() {
        PowerMockito.mockStatic(Hawk::class.java)
    }

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Test
    fun initTest() {
        val mockFireStore: FirebaseFirestore = mock()

        val viewModel = ViewProfileViewModel(mockFireStore)
        assertTrue(viewModel.userProfileData.value == null)
        assertTrue(viewModel.waitingDialogMsg.value == null)
        assertTrue(!viewModel.isProfileExists.get())
    }

    @Test
    fun getter_setter_test() {
        val mockFireStore: FirebaseFirestore = mock()

        val viewModel = ViewProfileViewModel(mockFireStore)
        val userProfile = UserProfile()
        userProfile.displayName = "test"
        viewModel.isProfileExists.set(true)
        viewModel.userProfileData.postValue(userProfile)
        viewModel.waitingDialogMsg.postValue("testing")

        assertTrue(viewModel.isProfileExists.get())
        assertNotNull(viewModel.userProfileData)
        assertNotNull(viewModel.waitingDialogMsg)
        val ret = viewModel.userProfileData.value
        assertEquals(ret?.displayName, "test")
        assertEquals(viewModel.waitingDialogMsg.value, "testing")
    }

    @Test
    fun eventsTest() {
        val mockFireStore: FirebaseFirestore = mock()
        val viewModel = ViewProfileViewModel(mockFireStore)

        var editClick = false
        var createClick = false

        viewModel.onEventReceived += { event ->
            when (event.type) {
                EventIdentifier.EDIT_PROFILE_CLICKED -> editClick = true
                EventIdentifier.CREATE_PROFILE_CLICKED -> createClick = true
                else -> {
                }
            }
        }

        viewModel.onEditProfileClick()
        assertTrue(editClick)

        viewModel.onCreateProfileClicked()
        assertTrue(createClick)
    }
}