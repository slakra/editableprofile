package com.example.editableprofile.viewModel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import com.example.editableprofile.apiManager.ApiManager
import com.example.editableprofile.common.util.PreferenceUtil
import com.example.editableprofile.database.dao.ChoiceDao
import com.example.editableprofile.database.dao.CityDao
import com.example.editableprofile.database.repository.ChoiceRepository
import com.example.editableprofile.database.repository.CityRepository
import com.example.editableprofile.model.City
import com.example.editableprofile.model.EventIdentifier
import com.example.editableprofile.network.IApiService
import com.example.editableprofile.ui.viewModel.EditProfileViewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.StorageReference
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.orhanobut.hawk.Hawk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.setMain
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.junit.MockitoJUnitRunner
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import org.powermock.modules.junit4.PowerMockRunnerDelegate

@RunWith(PowerMockRunner::class)
@PowerMockRunnerDelegate(MockitoJUnitRunner::class)
@PrepareForTest(PreferenceUtil::class, Hawk::class)
class EditProfileViewModelTest {

    @Before
    fun setUp() {
        PowerMockito.mockStatic(Hawk::class.java)
    }

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private fun sleep() {
        Thread.sleep(500)
    }

    @Test
    fun initTest() {
        val choiceDao: ChoiceDao = mock()
        val cityDao: CityDao = mock()
        val apiService: IApiService = mock()
        val apiManager = ApiManager(apiService, cityDao, choiceDao)
        val choiceRepository = ChoiceRepository(choiceDao, apiManager)
        val cityRepository = CityRepository(cityDao, apiManager)
        val mockFireStore: FirebaseFirestore = mock()
        val mockStorageRef: StorageReference = mock()

        val viewModel = EditProfileViewModel(choiceRepository, cityRepository, mockFireStore,
            mockStorageRef)
        assertTrue(viewModel.displayName.get() == "" && viewModel.realName.get() == "")
        assertTrue(viewModel.birthday.get() == "" && viewModel.gender.get() == "")
        assertTrue(viewModel.ethnicity.get() == "" && viewModel.religion.get() == "")
        assertTrue(viewModel.height.get() == "" && viewModel.figure.get() == "")
        assertTrue(viewModel.maritalStatus.get() == "" && viewModel.occupation.get() == "")
        assertTrue(viewModel.aboutMe.get() == "" && viewModel.location.get() == "")
        assertTrue(viewModel.profileUrl.get() == "")
        assertTrue(viewModel.isOnline)
        assertTrue(viewModel.isCreateProfile.get())
    }

    @Test
    fun getter_setter_test() {
        val choiceDao: ChoiceDao = mock()
        val cityDao: CityDao = mock()
        val apiService: IApiService = mock()
        val apiManager = ApiManager(apiService, cityDao, choiceDao)
        val choiceRepository = ChoiceRepository(choiceDao, apiManager)
        val cityRepository = CityRepository(cityDao, apiManager)
        val mockFireStore: FirebaseFirestore = mock()
        val mockStorageRef: StorageReference = mock()

        val viewModel = EditProfileViewModel(choiceRepository, cityRepository, mockFireStore,
            mockStorageRef)
        viewModel.displayName.set("displayName")
        viewModel.realName.set("realName")
        viewModel.birthday.set("birthday")
        viewModel.gender.set("gender")
        viewModel.ethnicity.set("ethnicity")
        viewModel.religion.set("religion")
        viewModel.height.set("123")
        viewModel.figure.set("figure")
        viewModel.maritalStatus.set("married")
        viewModel.occupation.set("occupation")
        viewModel.aboutMe.set("aboutMe")
        viewModel.location.set("location")
        viewModel.profileUrl.set("profile")
        viewModel.isOnline = false
        viewModel.isCreateProfile.set(false)
        viewModel.errorLocation.set(1)
        viewModel.errorBirthday.set(2)
        viewModel.errorRealName.set(3)
        viewModel.errorDisplayName.set(4)
        viewModel.waitingDialogMsg.postValue("testing")

        assertTrue(viewModel.displayName.get() == "displayName" && viewModel.realName.get() == "realName")
        assertTrue(viewModel.birthday.get() == "birthday" && viewModel.gender.get() == "gender")
        assertTrue(viewModel.ethnicity.get() == "ethnicity" && viewModel.religion.get() == "religion")
        assertTrue(viewModel.height.get() == "123" && viewModel.figure.get() == "figure")
        assertTrue(viewModel.maritalStatus.get() == "married" && viewModel.occupation.get() == "occupation")
        assertTrue(viewModel.aboutMe.get() == "aboutMe" && viewModel.location.get() == "location")
        assertTrue(viewModel.profileUrl.get() == "profile")
        assertFalse(viewModel.isOnline)
        assertFalse(viewModel.isCreateProfile.get())
        assertTrue(viewModel.errorLocation.get() == 1 && viewModel.errorBirthday.get() == 2)
        assertTrue(viewModel.errorRealName.get() == 3 && viewModel.errorDisplayName.get() == 4)
        assertNotNull(viewModel.waitingDialogMsg)
        assertEquals(viewModel.waitingDialogMsg.value, "testing")
    }

    @Test
    fun eventTest() {
        val choiceDao: ChoiceDao = mock()
        val cityDao: CityDao = mock()
        val apiService: IApiService = mock()
        val apiManager = ApiManager(apiService, cityDao, choiceDao)
        val choiceRepository = ChoiceRepository(choiceDao, apiManager)
        val cityRepository = CityRepository(cityDao, apiManager)
        val mockFireStore: FirebaseFirestore = mock()
        val mockStorageRef: StorageReference = mock()

        val viewModel = EditProfileViewModel(choiceRepository, cityRepository, mockFireStore,
            mockStorageRef)
        var selectPic = false
        var birthDayPicker = false
        viewModel.onEventReceived += { event ->
            when (event.type) {
                EventIdentifier.SELECT_PICTURE -> selectPic = true
                EventIdentifier.SHOW_BIRTHDAY_PICKER -> birthDayPicker = true
                else -> {
                }
            }
        }
        viewModel.selectPicture()
        assertTrue(selectPic)

        viewModel.showBirthdayPicker()
        assertTrue(birthDayPicker)
    }

    @Test
    fun getCitiesTest() {
        val choiceDao: ChoiceDao = mock()
        val cityDao: CityDao = mock()
        val apiService: IApiService = mock()
        val apiManager = ApiManager(apiService, cityDao, choiceDao)
        val choiceRepository = ChoiceRepository(choiceDao, apiManager)
        val cityRepository = CityRepository(cityDao, apiManager)
        val mockFireStore: FirebaseFirestore = mock()
        val mockStorageRef: StorageReference = mock()

        val viewModel = EditProfileViewModel(choiceRepository, cityRepository, mockFireStore,
            mockStorageRef)

        val list = ArrayList<City>()
        list.add(City("123", "234", "city1"))
        val mockData: LiveData<List<City>> = mock()

        whenever(cityDao.getCityList()).thenReturn(mockData)
        whenever(mockData.value).thenReturn(list)

        val ret = viewModel.getCities()
        assertTrue(ret.value?.size == 1)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getCitiesFromNetworkTest() {
        val choiceRepository: ChoiceRepository = mock()
        val cityRepository: CityRepository = mock()
        val mockFireStore: FirebaseFirestore = mock()
        val mockStorageRef: StorageReference = mock()

        val viewModel = EditProfileViewModel(choiceRepository, cityRepository, mockFireStore,
            mockStorageRef)
        runBlocking {
            viewModel.getCityListFromNetwork()
            sleep()
            verify(cityRepository, times(1)).getCities()
        }
    }

    @Test
    fun getSingleChoiceListTest() {
        val choiceRepository: ChoiceRepository = mock()
        val cityRepository: CityRepository = mock()
        val mockFireStore: FirebaseFirestore = mock()
        val mockStorageRef: StorageReference = mock()

        val viewModel = EditProfileViewModel(choiceRepository, cityRepository, mockFireStore,
            mockStorageRef)
        val mockList = ArrayList<String>()
        mockList.add("test1")
        mockList.add("test2")
        val mockLiveData: LiveData<List<String>> = mock()
        whenever(choiceRepository.getChoiceList(anyString())).thenReturn(mockLiveData)
        whenever(mockLiveData.value).thenReturn(mockList)

        var ret = viewModel.getGenderList()
        assertTrue(ret.value?.size == 2)

        ret = viewModel.getEthnicityList()
        assertTrue(ret.value?.size == 2)

        ret = viewModel.getFigureList()
        assertTrue(ret.value?.size == 2)

        ret = viewModel.getMaritalStatusList()
        assertTrue(ret.value?.size == 2)

        ret = viewModel.getReligionList()
        assertTrue(ret.value?.size == 2)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getSingleChoicesTest() {
        val choiceRepository: ChoiceRepository = mock()
        val cityRepository: CityRepository = mock()
        val mockFireStore: FirebaseFirestore = mock()
        val mockStorageRef: StorageReference = mock()
        val viewModel = EditProfileViewModel(choiceRepository, cityRepository, mockFireStore,
            mockStorageRef)

        runBlocking {
            Dispatchers.setMain(Dispatchers.IO)
            viewModel.getSingleChoices()
            sleep()
            verify(choiceRepository, times(1)).clearPreviousChoices()
            verify(choiceRepository, times(1)).getChoices()
        }
    }
}