package com.example.editableprofile.apiManager

import android.util.Log
import com.example.editableprofile.database.dao.ChoiceDao
import com.example.editableprofile.database.dao.CityDao
import com.example.editableprofile.model.ChoiceAttr
import com.example.editableprofile.model.City
import com.example.editableprofile.model.response.CityResponse
import com.example.editableprofile.model.response.SingleChoiceResponse
import com.example.editableprofile.network.IApiService
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner

@ExperimentalCoroutinesApi
@RunWith(PowerMockRunner::class)
@PrepareForTest(Log::class)
class ApiManagerTest {

    @Suppress("UnsafeCast")
    @Before
    fun setUp() {
        PowerMockito.mockStatic(Log::class.java)
        Mockito.`when`(Log.e(any(), any())).then {
            println(it.arguments[1] as String)
        }
    }

    @Test
    fun getCitiesTest() = runBlocking {
        val apiService: IApiService = mock()
        val cityDao: CityDao = mock()
        val choiceDao: ChoiceDao = mock()
        val apiManager = ApiManager(apiService, cityDao, choiceDao)

        var ans = 0

        val result: Deferred<CityResponse> = mock()
        whenever(apiService.getCities()).thenReturn(result)
        whenever(result.join()).then {
            ans = 2
            ans
        }

        val cityList = ArrayList<City>()
        cityList.add(City())
        val cityResponse = CityResponse(cityList)
        whenever(result.getCompleted()).thenReturn(cityResponse)

        val ret = apiManager.getCities()
        Assert.assertTrue(ret)
    }

    @Test
    fun getCitiesNegativeTest() = runBlocking {
        val apiService: IApiService = mock()
        val cityDao: CityDao = mock()
        val choiceDao: ChoiceDao = mock()
        val apiManager = ApiManager(apiService, cityDao, choiceDao)

        var ans = 0
        val result: Deferred<CityResponse> = mock()
        whenever(apiService.getCities()).thenReturn(result)
        whenever(result.join()).then {
            ans = 1
            ans
        }
        whenever(result.getCompleted()).thenReturn(null)
        val ret = apiManager.getCities()
        Assert.assertFalse(ret)
    }

    @Test
    fun getSingleChoicesTest() = runBlocking {
        val apiService: IApiService = mock()
        val cityDao: CityDao = mock()
        val choiceDao: ChoiceDao = mock()
        val apiManager = ApiManager(apiService, cityDao, choiceDao)

        var ans = 0

        val result: Deferred<SingleChoiceResponse> = mock()
        whenever(apiService.getSingleChoices()).thenReturn(result)
        whenever(result.join()).then {
            ans = 2
            ans
        }
        val genderList = ArrayList<ChoiceAttr>()
        genderList.add(ChoiceAttr("123", "male"))
        val singleChoiceResponse: SingleChoiceResponse = mock()
        whenever(result.getCompleted()).thenReturn(singleChoiceResponse)
        whenever(singleChoiceResponse.gender).thenReturn(genderList)

        apiManager.getSingleChoices()

        Assert.assertTrue(ans == 2)
    }

    @Test
    fun getSingleChoicesNegativeTest() = runBlocking {
        val apiService: IApiService = mock()
        val cityDao: CityDao = mock()
        val choiceDao: ChoiceDao = mock()
        val apiManager = ApiManager(apiService, cityDao, choiceDao)

        var ans = 0

        val result: Deferred<SingleChoiceResponse> = mock()
        whenever(apiService.getSingleChoices()).thenReturn(result)
        whenever(result.join()).then {
            ans = 2
            ans
        }
        val genderList = ArrayList<ChoiceAttr>()
        genderList.add(ChoiceAttr("123", "male"))
        val singleChoiceResponse: SingleChoiceResponse = mock()
        whenever(result.getCompleted()).thenReturn(null)

        val ret = apiManager.getSingleChoices()

        Assert.assertFalse(ret)
    }
}