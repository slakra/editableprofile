package com.example.editableprofile.di

import com.example.editableprofile.apiManager.ApiManager
import com.example.editableprofile.database.EditProfileDataBase
import com.example.editableprofile.database.dao.ChoiceDao
import com.example.editableprofile.database.dao.CityDao
import com.example.editableprofile.database.repository.ChoiceRepository
import com.example.editableprofile.database.repository.CityRepository
import com.example.editableprofile.network.IApiService
import com.example.editableprofile.ui.viewModel.EditProfileViewModel
import com.example.editableprofile.ui.viewModel.ViewProfileViewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.StorageReference
import com.orhanobut.hawk.Hawk
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.standalone.get
import org.koin.test.AutoCloseKoinTest
import org.koin.test.declareMock
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner

/**
 * DI module test cases
 * @author slakra
 * @date 09/02/2019
 */
@RunWith(PowerMockRunner::class)
@PrepareForTest(Hawk::class)
class DiModulesTest : AutoCloseKoinTest() {

    @Test
    fun repository_module_test() {
        startKoin(listOf(databaseModule, repositoryModule, managerModule, networkModule))
        val cityDao = declareMock<CityDao> { }
        val choiceDao = declareMock<ChoiceDao> { }

        declareMock<EditProfileDataBase> { }
        declareMock<ApiManager> { }
        val choiceRepository = get<ChoiceRepository>()
        assertNotNull(choiceRepository)
        assertTrue(choiceRepository.choiceDao == choiceDao)

        val cityRepository = get<CityRepository>()
        assertNotNull(cityRepository)
        assertTrue(cityRepository.cityDao == cityDao)
    }

    @Test
    fun manager_module_test() {
        startKoin(listOf(databaseModule, networkModule, managerModule))
        val cityDao = declareMock<CityDao> { }
        val choiceDao = declareMock<ChoiceDao> { }
        val apiService = declareMock<IApiService> { }

        declareMock<EditProfileDataBase> { }

        val apiManager = get<ApiManager>()
        assertNotNull(apiManager)
        assertTrue(apiManager.apiService == apiService)
        assertTrue(apiManager.cityDao == cityDao)
        assertTrue(apiManager.choiceDao == choiceDao)
    }

    @Test
    fun view_model_module_test() {
        startKoin(appModule)
        PowerMockito.mockStatic(Hawk::class.java)

        declareMock<CityDao> { }
        declareMock<ChoiceDao> { }
        declareMock<ApiManager> { }
        declareMock<FirebaseFirestore> { }
        declareMock<StorageReference> { }

        val editProfileVM = get<EditProfileViewModel>()
        assertNotNull(editProfileVM)

        val viewProfileVM = get<ViewProfileViewModel>()
        assertNotNull(viewProfileVM)
    }
}