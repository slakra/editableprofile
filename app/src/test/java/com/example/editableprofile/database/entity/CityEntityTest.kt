package com.example.editableprofile.database.entity

import org.junit.Assert
import org.junit.Test

class CityEntityTest {

    @Test
    fun getter_setter_test() {

        val cityEntity = CityEntity()
        cityEntity.city = "Delhi"
        cityEntity.lat = "123"
        cityEntity.lon = "234"

        Assert.assertTrue(cityEntity.city == "Delhi")
        Assert.assertTrue(cityEntity.lat == "123")
        Assert.assertTrue(cityEntity.lon == "234")
    }
}