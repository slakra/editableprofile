package com.example.editableprofile.database.entity

import org.junit.Assert
import org.junit.Test

class ChoiceEntityTest {

    @Test
    fun getter_setter_test() {
        val choiceEntity = ChoiceEntity()
        choiceEntity.category = "GENDER"
        choiceEntity.categoryId = "123"
        choiceEntity.name = "male"

        Assert.assertTrue(choiceEntity.category == "GENDER")
        Assert.assertTrue(choiceEntity.categoryId == "123")
        Assert.assertTrue(choiceEntity.name == "male")
    }
}