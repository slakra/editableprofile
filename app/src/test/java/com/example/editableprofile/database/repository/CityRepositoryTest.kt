package com.example.editableprofile.database.repository

import androidx.lifecycle.LiveData
import com.example.editableprofile.apiManager.ApiManager
import com.example.editableprofile.database.dao.CityDao
import com.example.editableprofile.model.City
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertTrue
import org.junit.Test

class CityRepositoryTest {

    @Test
    fun getCityListTest() {
        val cityDao: CityDao = mock()
        val apiManager: ApiManager = mock()
        val mockList = ArrayList<City>()
        mockList.add(City("234", "123", "city1"))
        mockList.add(City("234", "123", "city2"))
        val mockLiveData: LiveData<List<City>> = mock()
        whenever(cityDao.getCityList()).thenReturn(mockLiveData)
        whenever(mockLiveData.value).thenReturn(mockList)

        val repo = CityRepository(cityDao, apiManager)

        val ret = repo.getCityList()
        assertTrue(ret.value?.size == 2)
    }

    @Test
    fun getEmptyCityListTest() {
        val cityDao: CityDao = mock()
        val apiManager: ApiManager = mock()
        val mockLiveData: LiveData<List<City>> = mock()
        whenever(cityDao.getCityList()).thenReturn(mockLiveData)
        whenever(mockLiveData.value).thenReturn(ArrayList<City>())
        val repo = CityRepository(cityDao, apiManager)

        val ret = repo.getCityList()
        assertTrue(ret.value?.size == 0)
    }

    @Test
    @ExperimentalCoroutinesApi
    fun getCitiesTest() = runBlocking {
        val cityDao: CityDao = mock()
        val apiManager: ApiManager = mock()
        val repo = CityRepository(cityDao, apiManager)
        whenever(apiManager.getCities()).thenReturn(true)

        val ret = repo.getCities()
        assertTrue(ret)
    }
}
