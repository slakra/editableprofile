package com.example.editableprofile.database.repository

import androidx.lifecycle.LiveData
import com.example.editableprofile.apiManager.ApiManager
import com.example.editableprofile.database.dao.ChoiceDao
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class ChoiceRepositoryTest {

    @Test
    fun getChoiceListTest() {
        val choiceDao: ChoiceDao = mock()
        val apiManager: ApiManager = mock()
        val mockList = ArrayList<String>()
        mockList.add("test1")
        mockList.add("test2")
        val mockLiveData: LiveData<List<String>> = mock()
        whenever(choiceDao.getCategoryList(any())).thenReturn(mockLiveData)
        whenever(mockLiveData.value).thenReturn(mockList)

        val repo = ChoiceRepository(choiceDao, apiManager)

        val ret = repo.getChoiceList("test")
        Assert.assertTrue(ret.value?.size == 2)
    }

    @Test
    fun getEmptyChoiceListTest() {
        val choiceDao: ChoiceDao = mock()
        val apiManager: ApiManager = mock()
        val mockLiveData: LiveData<List<String>> = mock()
        whenever(choiceDao.getCategoryList(any())).thenReturn(mockLiveData)
        whenever(mockLiveData.value).thenReturn(ArrayList<String>())

        val repo = ChoiceRepository(choiceDao, apiManager)

        val ret = repo.getChoiceList("test")
        Assert.assertTrue(ret.value?.size == 0)
    }

    @Test
    @ExperimentalCoroutinesApi
    fun getChoicesTest() = runBlocking {
        val choiceDao: ChoiceDao = mock()
        val apiManager: ApiManager = mock()
        val repo = ChoiceRepository(choiceDao, apiManager)

        whenever(apiManager.getSingleChoices()).thenReturn(true)
        val ret = repo.getChoices()
        Assert.assertTrue(ret)
    }

    @Test
    fun clearPreviousChoicesTest() {
        val choiceDao: ChoiceDao = mock()
        val apiManager: ApiManager = mock()
        val repo = ChoiceRepository(choiceDao, apiManager)

        repo.clearPreviousChoices()
        verify(choiceDao, times(1)).deleteAllChoices()
    }
}