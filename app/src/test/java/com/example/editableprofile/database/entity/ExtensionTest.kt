package com.example.editableprofile.database.entity

import com.example.editableprofile.database.helper.toDBEntity
import com.example.editableprofile.database.helper.toModel
import com.example.editableprofile.model.ChoiceAttr
import com.example.editableprofile.model.City
import org.junit.Assert.assertTrue
import org.junit.Test

class ExtensionTest {

    @Test
    fun cast_city_to_db_entity_test() {
        val city = City("123", "234", "Delhi")

        val ret = city.toDBEntity()

        assertTrue(ret.city == "Delhi")
        assertTrue(ret.lat == "123")
        assertTrue(ret.lon == "234")
    }

    @Test
    fun cast_city_entity_to_model_test() {
        val cityEntity = CityEntity()
        cityEntity.lon = "123"
        cityEntity.lat = "234"
        cityEntity.city = "Delhi"

        val ret = cityEntity.toModel()
        assertTrue(ret.lon == "123")
        assertTrue(ret.lat == "234")
        assertTrue(ret.city == "Delhi")
    }

    @Test
    fun cast_choice_attr_to_db_entity_test() {
        val choiceAttr = ChoiceAttr("123", "male")

        val ret = choiceAttr.toDBEntity("gender")
        assertTrue(ret.category == "gender")
        assertTrue(ret.categoryId == "123")
        assertTrue(ret.name == "male")
    }

    @Test
    fun cast_choice_entity_to_model_test() {
        val choiceEntity = ChoiceEntity()
        choiceEntity.name = "female"
        choiceEntity.categoryId = "123"
        choiceEntity.category = "gender"

        val ret = choiceEntity.toModel()
        assertTrue(ret.name == "female")
        assertTrue(ret.id == "123")
    }
}
