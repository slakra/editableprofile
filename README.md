# EditableProfile Mobile App
- - - -

## Business Requirement

  As an user I want to able to create, view and edit my profile at both App side and server end. Also profile image should be uploaded on the sever.

## Libraries Used
  
  * Retrofit
  * Koin
  * Mockito
  * Glide
  * Room
  * Espresso
  * Coroutine
  * RxJava
  * Firebase
  
## User Requirements
   
   * Create profile
   * Get city list and Single choice from server
   * View Profile with allowed attributes
   * Edit/Update profile attributes along with profile image
   
## Things done in project
   
   Project is developed using MVVM clean architecture and in Kotlin language. App is following offline first architecture. Room DB is used for offline caching of Cities list and various Single choice attributes. App is continuously monitoring the local storage and only fetch data from Api if data not available locally. For User profile data Firestore is used with local caching. For profile image Firebase Storage is used.
   
## Installation
   
   * Android Studio 3.5
   * Android SDK(28)
   * clone the project using ** git clone https://slakra@bitbucket.org/slakra/editableprofile.git **
   * Change the IP_ADDRESS in **NetworkModule.kt** with your machine's **IPv4** address
###   `NetworkModule.kt`
   ``` private const val IP_ADDRESS = "192.168.0.106:8080"
   ```
   
   * To get the ipv4 of your machine open **command prompt** and type **ipconfig** and replace the current **IP_ADDRESS** with your machine's **IPv4** address
   * Start the server following the commands from the coding exercise https://github.com/sparknetworks/coding_exercises_options/tree/master/editable_profile
   
## App Screenshots
----
![1](https://user-images.githubusercontent.com/34394394/64924258-31e1ec00-d800-11e9-9f83-6aaaf366ffe1.png)
![2](https://user-images.githubusercontent.com/34394394/64924304-9a30cd80-d800-11e9-9f38-68edf2de4891.png)
![3](https://user-images.githubusercontent.com/34394394/64924312-afa5f780-d800-11e9-92f6-70c59f077438.png)
![4](https://user-images.githubusercontent.com/34394394/64924317-c8161200-d800-11e9-88a9-edf8fcbdcebe.png)
  
  


